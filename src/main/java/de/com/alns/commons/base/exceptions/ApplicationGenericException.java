package de.com.alns.commons.base.exceptions;

import org.apache.commons.lang.exception.NestableRuntimeException;

public class ApplicationGenericException extends NestableRuntimeException {

	private static final long serialVersionUID = 1L;


	public ApplicationGenericException(Exception pException) {
		super(pException);
	}

	public ApplicationGenericException(String pMessage) {
		super(pMessage);
	}


	public ApplicationGenericException(String pMessage, Exception pExc) {
		super(pMessage, pExc);
	}


}
