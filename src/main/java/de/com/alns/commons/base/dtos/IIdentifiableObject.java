package de.com.alns.commons.base.dtos;


/**
 * Todas as classes persistentes ou DTOs implementam (ou melhor, devem implementar) esta interface. Ela �
 * usada para auxiliar a indicar a todas as APIs necess�rias que trata-se de uma classe
 * persistente.
 *
 * @since 1.2.0
 */
public interface IIdentifiableObject<TObjPK> {

    /**
     * Retorna o ID do objeto persistente.
     *
     * @since 1.2.0
     * @return
     */
    public TObjPK getId();

    /**
     * Atribui o ID do objeto persistente.
     *
     * @since 1.2.0
     * @param pId
     */
    public void setId(TObjPK pId);


}
