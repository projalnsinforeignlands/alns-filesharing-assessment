package de.com.alns.commons.base.models;


import de.com.alns.commons.base.dtos.IIdentifiableObject;

import java.io.Serializable;


/**
 * Basic class for Persistent Enteties.
 *
 */
public abstract class AbstractPersistentModel<TObjPK> implements Serializable, Cloneable, IIdentifiableObject<TObjPK> {

    private static final long serialVersionUID = -2580226132374114758L;

    public AbstractPersistentModel() {

    }


    public abstract TObjPK getId();
    public abstract void setId(TObjPK pId);


    /*
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
    */

}