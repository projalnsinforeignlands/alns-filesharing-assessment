package de.com.alns.commons.base.dtos;


import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;


/**
 * Classe básica dos DTO's de transporte.
 *
 */
public abstract class AbstractBaseDTO implements Serializable, Cloneable {

    private static final long serialVersionUID = -2580226132374114758L;

    public AbstractBaseDTO() {

    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


}