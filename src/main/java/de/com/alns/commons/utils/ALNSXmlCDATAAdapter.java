package de.com.alns.commons.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;


public class ALNSXmlCDATAAdapter extends XmlAdapter<String, String> {


    public ALNSXmlCDATAAdapter() {

    }


    @Override
    public String marshal(String arg0) throws Exception {
        return "<![CDATA[" + arg0 + "]]>";
    }

    @Override
    public String unmarshal(String arg0) throws Exception {
        return arg0;
    }


}
