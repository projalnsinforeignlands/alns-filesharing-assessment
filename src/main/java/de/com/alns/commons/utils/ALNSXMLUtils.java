package de.com.alns.commons.utils;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

public class ALNSXMLUtils {


	public static <T> JAXBElement<T> createJAXBElement(String pDefaultNamespace, String pQNameAsStr, Class<T> pClazzType, T pValueElement) {

		JAXBElement<T> elementResult = null;

		elementResult = new JAXBElement<T>(new QName(pDefaultNamespace, pQNameAsStr), pClazzType, pValueElement);

		return elementResult;
	}
}
