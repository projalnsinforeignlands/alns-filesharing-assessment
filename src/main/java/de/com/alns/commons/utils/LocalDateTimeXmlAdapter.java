package de.com.alns.commons.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeXmlAdapter extends XmlAdapter<String, LocalDateTime> {

	@Override
	public LocalDateTime unmarshal(String pDatetimeAsString) throws Exception {

		LocalDateTime ldtResult = null;

		if (pDatetimeAsString != null) {
			ldtResult = DateUtils.convertStringToDateTime(pDatetimeAsString, DateTimeFormatter.ISO_OFFSET_DATE_TIME, null);
		}
		return ldtResult;
	}

	@Override
	public String marshal(LocalDateTime v) throws Exception {
		return v.toString();
	}

}	