package de.com.alns.commons.utils;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {


	public static final int TIME_UNIT_IN_MILISECONDS = 0;
	public static final int TIME_UNIT_IN_SECONDS = 1;
	public static final int TIME_UNIT_IN_MINUTES = 2;
	public static final int TIME_UNIT_IN_HOURS = 3;
	public static final int TIME_UNIT_IN_DAYS = 4;
	public static final int TIME_UNIT_IN_WEEKS = 5;
	public static final int TIME_UNIT_IN_MONTHS = 6;
	public static final int TIME_UNIT_IN_YEARS = 7;



	public static String convertDateToStringByPattern(Date pDateSource, String pPatternFormat) {

		String strResult = null;
		DateFormat dateFormatTarget = null;

		dateFormatTarget = new SimpleDateFormat(pPatternFormat);
		strResult = dateFormatTarget.format(pDateSource);

		return strResult;
	}


	public static LocalDateTime convertStringToDateTime(String pDatetimeAsString, DateTimeFormatter pDateTimeFrmToUse, String pDatePattern) {

		LocalDateTime ldtResult = null;
		DateTimeFormatter dtFormater = null;

		if (pDateTimeFrmToUse != null) {
			dtFormater = pDateTimeFrmToUse;
		} else if (pDatePattern != null) {
			dtFormater = DateTimeFormatter.ofPattern(pDatePattern);
		} else {
			dtFormater = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
		}

		ldtResult = LocalDateTime.parse(pDatetimeAsString, dtFormater);

		return ldtResult;
	}


	public static Date convertStringDateToDateWithoutTime(String pDateInString, String pDatePattern ) {

		Date dateResult = null;
		SimpleDateFormat sdtFormatter = new SimpleDateFormat(pDatePattern);

		try {

			dateResult = sdtFormatter.parse(pDateInString);

		} catch (java.text.ParseException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return dateResult;

	}


	public static XMLGregorianCalendar convertStringDateToXmlDate(String pDateInString, String pDatePattern) {

		return convertDateToXmlDate(convertStringDateToDateWithoutTime(pDateInString, pDatePattern));
	}


	public static XMLGregorianCalendar convertDateToXmlDate(Date date) {

		XMLGregorianCalendar xmlDate = null;
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);

		try {

			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);

		} catch(Exception e) {
			e.printStackTrace();
		}

		return   xmlDate;
	}

	public static Integer calculateQtySomeTimeUnitBetweenDates(Integer pTimeUnitToConsider, Date pDateStart, Date pDateEnd) {

		Integer intResult = -1;
		LocalDateTime ldtStart = null;
		LocalDateTime ldtEnd = null;
		LocalDateTime ldtAux = null;

		ldtStart = LocalDateTime.ofInstant(pDateStart.toInstant(), ZoneId.systemDefault());

		if (pDateEnd != null) {
			ldtEnd = LocalDateTime.ofInstant(pDateEnd.toInstant(), ZoneId.systemDefault());
		} else {
			ldtEnd = LocalDateTime.now();
		}

		if (ldtStart.isAfter(ldtEnd)) {
			ldtAux = ldtStart;
			ldtStart = ldtEnd;
			ldtEnd = ldtAux;
		}

		switch (pTimeUnitToConsider) {
			case TIME_UNIT_IN_DAYS :
				intResult = Math.toIntExact(ChronoUnit.DAYS.between(ldtStart, ldtEnd));
				break;
			case TIME_UNIT_IN_WEEKS:
				intResult = Math.toIntExact(ChronoUnit.WEEKS.between(ldtStart, ldtEnd));
				break;
			default:
				intResult = Math.toIntExact(ChronoUnit.MINUTES.between(ldtStart, ldtEnd));
				break;
		}

		return  intResult;
	}


    public static Date addTimeUnits(Date pDateSource, Long pQtdUnitsTimeToAdd, Integer pTimeUnitToConsider) {

		Date dateResult = null;
		LocalDateTime ldtStart = null;

		ldtStart = LocalDateTime.ofInstant(pDateSource.toInstant(), ZoneId.systemDefault());

		switch (pTimeUnitToConsider) {
			case TIME_UNIT_IN_MINUTES :
				ldtStart = ldtStart.plusMinutes(pQtdUnitsTimeToAdd);
				break;
			case TIME_UNIT_IN_DAYS :
				ldtStart = ldtStart.plusDays(pQtdUnitsTimeToAdd);
				break;
			case TIME_UNIT_IN_WEEKS:
				ldtStart = ldtStart.plusWeeks(pQtdUnitsTimeToAdd);
				break;
			default:
				ldtStart = ldtStart.plusMonths(pQtdUnitsTimeToAdd);
				break;
		}

		dateResult = Date.from(ldtStart.atZone(ZoneId.systemDefault()).toInstant());

		return dateResult;

    }
}
