package de.com.alns.commons.utils;

import com.sun.org.apache.xerces.internal.dom.ElementNSImpl;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXB;
import javax.xml.transform.Result;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlClassMapperUtils<TObjTarget> {

    private Class<TObjTarget> targetObjectClazz;


    public XmlClassMapperUtils(Class<TObjTarget> pTargetObjectClazz) {

        this.targetObjectClazz = pTargetObjectClazz;
    }


    public Object buildAndMapXmlToObject(String pXmlDataPayload, String pRootName) {

        Object objectResult = null;
        String strXmlFake = null;
        StringReader strdrReaderHelper = null;

        strXmlFake = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
                     (StringUtils.isEmpty(pRootName) ? "" : "<" + pRootName + ">") +
                     pXmlDataPayload +
                     (StringUtils.isEmpty(pRootName) ? "" : "</" + pRootName + ">");

        strdrReaderHelper = new StringReader(strXmlFake);

        objectResult = JAXB.unmarshal(strdrReaderHelper, this.targetObjectClazz);

        return objectResult;

    }


    public TObjTarget buildAndMapAnyTypeToListObject(ElementNSImpl pElementNSFromAnyType) {

        TObjTarget objectResult = null;

        DOMSource source = new DOMSource(pElementNSFromAnyType);

        objectResult = (TObjTarget) JAXB.unmarshal(source, this.targetObjectClazz);

        return objectResult;

    }


    public String buildXmlPayloadFromObject(TObjTarget pObjSourceToBeConvertedToXml) {

        String strResult = null;
        Result resultXml = null;
        StringWriter strWriter = null;

        if (pObjSourceToBeConvertedToXml != null) {

            strWriter = new StringWriter();
            resultXml = new StreamResult(strWriter);

            JAXB.marshal(pObjSourceToBeConvertedToXml, resultXml);

            strResult = strWriter.toString();

        }

        return strResult;

    }
}

