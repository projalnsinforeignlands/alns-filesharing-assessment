package de.com.alns.codingtests.leanapps.filesharing.services.impl;

import de.com.alns.codingtests.leanapps.filesharing.models.User;
import de.com.alns.codingtests.leanapps.filesharing.services.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Sring Security Custom Authentication Provider to Use our UserService Bean
 */
@Component
public class ALNSFileSharingAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private IUserService iUserService;

    @Override
    public Authentication authenticate(Authentication pAuthentication) throws AuthenticationException {

        User userFound = null;
        List<GrantedAuthority> authorities = null;

        String email = pAuthentication.getName();
        String password = pAuthentication.getCredentials().toString();

        userFound = iUserService.searchUserByEmail(email).get();

        if (userFound == null) {
            throw new UsernameNotFoundException(email);
        } else {
            authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN")); // description is a string
        }

        return new UsernamePasswordAuthenticationToken(email, password, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}