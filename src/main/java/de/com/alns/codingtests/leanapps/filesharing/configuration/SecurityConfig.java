package de.com.alns.codingtests.leanapps.filesharing.configuration;

import de.com.alns.codingtests.leanapps.filesharing.services.impl.ALNSFileSharingAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ALNSFileSharingAuthenticationProvider authProvider;


    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.csrf()
                    .disable()
                    .authorizeRequests()
                    .antMatchers("/swagger-ui.html*/**").permitAll()
                    .antMatchers("/h2-console/**").permitAll()
                    .antMatchers("/register/**").permitAll()
                    .antMatchers("/api/**").authenticated()
                    .anyRequest().authenticated()
                    .and()
                    .httpBasic();

        httpSecurity.headers()
                .frameOptions()
                .sameOrigin();

        httpSecurity.logout()
                    .logoutUrl("/doLogout")
                    .logoutSuccessUrl("https://bitbucket.org/projalnsinforeignlands/alns-filesharing-assessment");

    }

}
