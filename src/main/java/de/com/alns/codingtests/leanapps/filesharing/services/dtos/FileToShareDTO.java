package de.com.alns.codingtests.leanapps.filesharing.services.dtos;

import de.com.alns.commons.base.dtos.AbstractBaseDTO;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A representation of a new User.
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FileToShareDTO extends AbstractBaseDTO {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Email
    @Size(min = 5, max = 255)
    @Getter @Setter
    private String emailGrantedUser;

    @NotNull
    @Getter @Setter
    private Long fileUploadedToShareId;


}
