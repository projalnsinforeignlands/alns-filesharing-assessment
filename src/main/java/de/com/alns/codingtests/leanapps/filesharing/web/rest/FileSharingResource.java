package de.com.alns.codingtests.leanapps.filesharing.web.rest;

import de.com.alns.codingtests.leanapps.filesharing.models.FileUploaded;
import de.com.alns.codingtests.leanapps.filesharing.services.IFileUploadedService;
import de.com.alns.codingtests.leanapps.filesharing.services.IUserService;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.FileToShareDTO;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.FilesAvailableForCurrentUserDTO;
import de.com.alns.commons.base.exceptions.ApplicationGenericException;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * REST controller for managing File Sharing.
 */
@RestController
@RequestMapping("/api")
public class FileSharingResource {

    private final Logger log = LoggerFactory.getLogger(FileSharingResource.class);

    private final IFileUploadedService iFileUploadedService;



    public FileSharingResource(IFileUploadedService pIFileUploadedService) {

        this.iFileUploadedService = pIFileUploadedService;
    }


    @GetMapping("/file")
    @Timed
    public ResponseEntity<FilesAvailableForCurrentUserDTO> getFilesAvailableByCurrentUser() {

        ResponseEntity<FilesAvailableForCurrentUserDTO> responseEntityResult = null;
        FilesAvailableForCurrentUserDTO filesAvailableForCurrentUserDTO = null;
        String currentUserId = null;

        try {

            currentUserId = getCurrentUserId();

            log.debug("REST request to getFilesAvailableByCurrentUser(): {}", currentUserId);

            filesAvailableForCurrentUserDTO = iFileUploadedService.mountAllFilesAvailableByCurrentUser(currentUserId);

            responseEntityResult = ResponseEntity.ok().body(filesAvailableForCurrentUserDTO);

        } catch (ApplicationGenericException e) {
            log.error(e.getMessage());
            responseEntityResult = ResponseEntity.badRequest().build();
        }

        return responseEntityResult;

    }


    @PostMapping("/file")
    @Timed
    public ResponseEntity<FileUploaded> uploadFile(@RequestParam("pFileToUpload") MultipartFile pFileToUpload) {

        ResponseEntity<FileUploaded> responseEntityResult = null;
        FileUploaded fileUploadedResult = null;
        String currentUserId = null;
        String contextAppUri = null;

        currentUserId = getCurrentUserId();

        contextAppUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                                                   .path("/api")
                                                   .path("/file")
                                                   .toUriString();

        fileUploadedResult = iFileUploadedService.storeUploadedFile(currentUserId, contextAppUri, pFileToUpload);

        responseEntityResult = ResponseEntity.ok().body(fileUploadedResult);

        return responseEntityResult;
    }


    @GetMapping("/file/{pFileUploadedId}")
    @Timed
    public ResponseEntity<Resource> getFileToDownload(@PathVariable("pFileUploadedId") Long pFileUploadedId, HttpServletRequest pHttpReq) {

        ResponseEntity<Resource> responseEntityResult = null;
        Resource fileAsResource = null;
        String currentUserId = null;
        String contentType = null;

        try {

            log.debug("REST request to Download FileId : {}", pFileUploadedId);

            currentUserId = getCurrentUserId();

            fileAsResource = iFileUploadedService.loadFileAsResource(currentUserId, pFileUploadedId);

            if (fileAsResource != null) {

                // Try to determine file's content type
                contentType = null;

                try {

                    contentType = pHttpReq.getServletContext().getMimeType(fileAsResource.getFile().getAbsolutePath());

                } catch (IOException ex) {
                    log.info("Could not determine file type.");
                }

                // Fallback to the default content type if type could not be determined
                if (contentType == null) {
                    contentType = "application/octet-stream";
                }

                responseEntityResult = ResponseEntity.ok()
                                                     .contentType(MediaType.parseMediaType(contentType))
                                                     .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileAsResource.getFilename() + "\"")
                                                     .body(fileAsResource);
            }

        } catch (ApplicationGenericException e) {
            log.error(e.getMessage());
            responseEntityResult = ResponseEntity.badRequest().build();
        }

        return responseEntityResult;

    }


    @PostMapping("/share")
    @Timed
    public ResponseEntity<Void> shareFile(@RequestBody FileToShareDTO pFileToShareDTO) throws ApplicationGenericException {

        ResponseEntity<Void> responseEntityReturn = null;
        Integer intResult = null;

        try {

            log.debug("REST Post to Share a File #Id: " + pFileToShareDTO.getFileUploadedToShareId() + " To User #Email:" + pFileToShareDTO.getEmailGrantedUser());

            intResult = iFileUploadedService.shareFileOwnedByCurrentUserToGrantedUser(getCurrentUserId(), pFileToShareDTO);

            if (intResult == 1) {

                responseEntityReturn = ResponseEntity.created(new URI("/api/users/" + pFileToShareDTO.getEmailGrantedUser()))
                                                     .body(null);

            } else {

                responseEntityReturn = ResponseEntity.notFound().build();

            }
            
        } catch (URISyntaxException e) {
            log.debug(e.getMessage());
            throw new ApplicationGenericException(e);
        }

        return responseEntityReturn;
    }


    private String getCurrentUserId() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String loginUsername = auth.getName();

        return loginUsername;
    }


}
