package de.com.alns.codingtests.leanapps.filesharing.services.impl;

import de.com.alns.codingtests.leanapps.filesharing.models.FileUploaded;
import de.com.alns.codingtests.leanapps.filesharing.models.User;
import de.com.alns.codingtests.leanapps.filesharing.repositories.FileUploadedRepository;
import de.com.alns.codingtests.leanapps.filesharing.services.IFileUploadedService;
import de.com.alns.codingtests.leanapps.filesharing.services.IUserService;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.FileToShareDTO;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.FilesAvailableForCurrentUserDTO;
import de.com.alns.commons.base.exceptions.ApplicationGenericException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Service Implementation for managing FileUploaded.
 */
@Service
@Transactional
public class FileUploadedServiceImpl implements IFileUploadedService {

    private final Logger logger = LoggerFactory.getLogger(FileUploadedServiceImpl.class);

    private final FileUploadedRepository fileUploadedRepository;

    private final Path fileStorageLocation;

    @Autowired
    private IUserService iUserService;


    @Autowired
    public FileUploadedServiceImpl(FileUploadedRepository pFileUploadedRepository, 
                                   @Value("${com.demo.uploads.directory}") String pFromEnvCfgDefaultUploadDir) {

        try {

            this.fileUploadedRepository = pFileUploadedRepository;

            logger.info("==> Upload Dir to be used: " + pFromEnvCfgDefaultUploadDir);
            
            this.fileStorageLocation = Paths.get(pFromEnvCfgDefaultUploadDir).toAbsolutePath().normalize();

            Files.createDirectories(this.fileStorageLocation);

        } catch (Exception ex) {
            throw new ApplicationGenericException("Could not create the directory where the uploaded files will be stored.", ex);
        }

    }


    @Override
    public FilesAvailableForCurrentUserDTO mountAllFilesAvailableByCurrentUser(String pUserEmail) {

        FilesAvailableForCurrentUserDTO objResult = null;
        User userFound = null;

        userFound = iUserService.searchUserByEmail(pUserEmail).get();

        if (userFound != null) {

            objResult = new FilesAvailableForCurrentUserDTO();
            objResult.setEmail(userFound.getEmail());
            objResult.setOwnFilesUploadedList(userFound.getOwnFilesUploadedList());
            objResult.setFilesUploadedSharedWithMeList(userFound.getFilesUploadedSharedWithMeList());

        }

        return objResult;
    }


    @Override
    public FileUploaded storeUploadedFile(String pUserEmail, String pContextAppUriToDownload, MultipartFile pMultipartedFile) {

        FileUploaded fileUploadedSaved = null;
        User userOwner = null;
        String fileName = null;
        Path targetLocation = null;

        try {

            if (!StringUtils.isEmpty(pUserEmail) && pMultipartedFile != null && !pMultipartedFile.isEmpty()) {

                // Get Current User
                userOwner = iUserService.searchUserByEmail(pUserEmail).get();

                if (userOwner != null) {

                    // Normalize file name
                    fileName = StringUtils.cleanPath(pMultipartedFile.getOriginalFilename());

                    // Check if the file's name contains invalid characters
                    if (fileName.contains("..")) {
                        throw new ApplicationGenericException("Sorry! Filename contains invalid path sequence " + fileName);
                    }

                    // Copy file to the target location (Replacing existing file with the same name)
                    targetLocation = this.fileStorageLocation.resolve(fileName);
                    Files.copy(pMultipartedFile.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

                    fileUploadedSaved = new FileUploaded();
                    fileUploadedSaved.setFileName(fileName);
                    fileUploadedSaved.setQtyDownloads(0);
                    fileUploadedSaved.setActive(true);
                    fileUploadedSaved.setFilePath(targetLocation.toString());
                    fileUploadedSaved.setSize(pMultipartedFile.getSize());
                    fileUploadedSaved.setContentType(pMultipartedFile.getContentType());

                    fileUploadedSaved.setUserOwner(userOwner);

                    fileUploadedSaved = fileUploadedRepository.save(fileUploadedSaved);

                    fileUploadedSaved.setDownloadURI(pContextAppUriToDownload + "/" + fileUploadedSaved.getId());

                    fileUploadedSaved = fileUploadedRepository.save(fileUploadedSaved);

                }
            }

            return fileUploadedSaved;

        } catch (IOException ex) {
            throw new ApplicationGenericException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }


    @Override
    public Resource loadFileAsResource(String pUserEmail, Long pFileUploadedId) {

        Path filePath = null;
        Resource resourceResult = null;
        User userGrantedFound = null;
        FileUploaded fileUploadedFound = null;

       try {

            if (!StringUtils.isEmpty(pUserEmail)) {

                // Get Current User
                userGrantedFound = iUserService.searchUserByEmail(pUserEmail).get();

                if (userGrantedFound != null &&
                   (!CollectionUtils.isEmpty(userGrantedFound.getOwnFilesUploadedList()) ||
                    !CollectionUtils.isEmpty(userGrantedFound.getFilesUploadedSharedWithMeList()))) {

                    // Search the File in User's owned File collection
                    fileUploadedFound = userGrantedFound.getOwnFilesUploadedList().stream()
                            .filter(oneFileUpl -> pFileUploadedId.equals(oneFileUpl.getId()))
                            .findAny()
                            .orElse(null);

                    if (fileUploadedFound == null) {

                        // Search the File in collection of siles shared with Currrent User
                        fileUploadedFound = userGrantedFound.getFilesUploadedSharedWithMeList().stream()
                                .filter(oneFileUpl -> pFileUploadedId.equals(oneFileUpl.getId()))
                                .findAny()
                                .orElse(null);

                    }

                }

                if (fileUploadedFound != null && !StringUtils.isEmpty(fileUploadedFound.getFileName())) {

                    filePath = this.fileStorageLocation.resolve(fileUploadedFound.getFileName()).normalize();
                    resourceResult = new UrlResource(filePath.toUri());

                    if (resourceResult.exists()) {
                        fileUploadedFound.addQtyDownloads(1);
                        fileUploadedFound = fileUploadedRepository.save(fileUploadedFound);
                    }

                } else {
                    throw new ApplicationGenericException("File #Id: {" + pFileUploadedId + "} not found, not owned or not shared with the Current User #Id: {" + pUserEmail + "}");
                }

            }


        } catch (MalformedURLException ex) {
            throw new ApplicationGenericException("File #Id: {" + pFileUploadedId + "} doesn't exist in Path: " + filePath, ex);
        }

        return resourceResult;

    }


    @Override
    public Integer shareFileOwnedByCurrentUserToGrantedUser(String pUserEmail, FileToShareDTO pFileToShareDTO) {

        Integer intResultAction = 0;
        User userOwnerFound = null;
        User userGrantedFound = null;
        FileUploaded fileUploadedToShare = null;
        FileUploaded fileUploadedAux = null;

        try {

            if (!StringUtils.isEmpty(pUserEmail) && pFileToShareDTO != null) {

                // Get Owner User (Current User)
                userOwnerFound = iUserService.searchUserByEmail(pUserEmail).get();

                if (userOwnerFound != null &&
                    (!CollectionUtils.isEmpty(userOwnerFound.getOwnFilesUploadedList()) ||
                     !CollectionUtils.isEmpty(userOwnerFound.getFilesUploadedSharedWithMeList()))) {

                    // Search the File in User's owned File collection
                    fileUploadedToShare = userOwnerFound.getOwnFilesUploadedList().stream()
                                                        .filter(oneFileUpl -> pFileToShareDTO.getFileUploadedToShareId().equals(oneFileUpl.getId()))
                                                        .findAny()
                                                        .orElse(null);

                    if (fileUploadedToShare != null) {

                        // Get Owner User (Current User)
                        userGrantedFound = iUserService.searchUserByEmail(pFileToShareDTO.getEmailGrantedUser()).get();

                        if (userGrantedFound != null && !userGrantedFound.getFilesUploadedSharedWithMeList().contains(fileUploadedToShare)) {

                            userGrantedFound.getFilesUploadedSharedWithMeList().add(fileUploadedToShare);
                            userGrantedFound = iUserService.save(userGrantedFound);
                            intResultAction = 1;

                        }
                    }

                }

            }


        } catch (Exception ex) {
            throw new ApplicationGenericException("File #Id: {" + pFileToShareDTO.getFileUploadedToShareId() + "} couldn't be shared with User #Email: " + pFileToShareDTO.getEmailGrantedUser(), ex);
        }

        return intResultAction;

    }

}
