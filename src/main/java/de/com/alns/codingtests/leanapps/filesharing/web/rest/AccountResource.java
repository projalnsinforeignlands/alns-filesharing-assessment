package de.com.alns.codingtests.leanapps.filesharing.web.rest;

import de.com.alns.codingtests.leanapps.filesharing.models.User;
import de.com.alns.codingtests.leanapps.filesharing.services.IUserService;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.NewUserDTO;
import de.com.alns.commons.base.exceptions.ApplicationGenericException;
import de.com.alns.commons.utils.RestAPIUtils;
import io.micrometer.core.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * REST controller for managing User Account creation.
 */
@RestController
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final IUserService iUserService;

    public AccountResource(IUserService pIUserService) {
        this.iUserService = pIUserService;
    }


    /**
     * POST  /register  : Creates a new user.
     * @param pNewUserDTO the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request) if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/register")
    @Timed
    // @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> createUser(@Valid @RequestBody NewUserDTO pNewUserDTO) throws ApplicationGenericException {

        ResponseEntity<Void> responseEntityReturn = null;
        String newUserId = null;

        try {

            log.debug("REST request to save User : {}", pNewUserDTO);

            newUserId = iUserService.createUser(pNewUserDTO);

            responseEntityReturn = ResponseEntity.created(new URI("/api/users/" + newUserId))
                                                 .body(null);

        } catch (URISyntaxException e) {
            log.debug(e.getMessage());
            throw new ApplicationGenericException(e);
        }

        return responseEntityReturn;
    }


    @GetMapping("/api/users/{pUserId}")
    @Timed
    public ResponseEntity<User> getUserDetails(@PathVariable("pUserId") String pUserId) {

        ResponseEntity<User> responseEntityResult = null;
        Optional<User> optUserResult = null;

        try {

            log.debug("REST request to get UserId : {}", pUserId);

            optUserResult = iUserService.searchUserByEmail(pUserId);

            responseEntityResult = RestAPIUtils.wrapOrNotFound(optUserResult);

        } catch (ApplicationGenericException e) {
            log.error(e.getMessage());
            responseEntityResult = ResponseEntity.badRequest().build();
        }

        return responseEntityResult;

    }


}
