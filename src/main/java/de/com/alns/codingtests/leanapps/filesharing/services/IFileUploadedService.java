package de.com.alns.codingtests.leanapps.filesharing.services;

import de.com.alns.codingtests.leanapps.filesharing.models.FileUploaded;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.FileToShareDTO;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.FilesAvailableForCurrentUserDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * Service Interface for managing FileUploaded.
 */
public interface IFileUploadedService {

    public FilesAvailableForCurrentUserDTO mountAllFilesAvailableByCurrentUser(String pUserEmail);

    public FileUploaded storeUploadedFile(String pUserEmail, String pContextAppUriToDownload, MultipartFile pMultipartedFile);

    public Resource loadFileAsResource(String pUserEmail, Long pFileUploadedId);

    public Integer shareFileOwnedByCurrentUserToGrantedUser(String pUserEmail, FileToShareDTO pFileToShareDTO);
}
