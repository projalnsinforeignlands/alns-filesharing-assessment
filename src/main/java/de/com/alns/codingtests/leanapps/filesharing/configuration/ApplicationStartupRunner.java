package de.com.alns.codingtests.leanapps.filesharing.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import de.com.alns.codingtests.leanapps.filesharing.models.User;
import de.com.alns.codingtests.leanapps.filesharing.services.IUserService;

public class ApplicationStartupRunner implements CommandLineRunner {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IUserService iUserService;
    
    
    @Override
    public void run(String... args) throws Exception {
        
        User oneUserAdmin = null;

        logger.info("Application Started !! - Creating admin@system.com / 12345678 User.");

        oneUserAdmin = new User();
        oneUserAdmin.setEmail("admin@system.com");
        oneUserAdmin.setFirstName("Administrator");
        oneUserAdmin.setLastName("System Default");
        oneUserAdmin.setIsActive(true);
        oneUserAdmin.setPassword("12345678");
        oneUserAdmin = iUserService.save(oneUserAdmin);

    }
}