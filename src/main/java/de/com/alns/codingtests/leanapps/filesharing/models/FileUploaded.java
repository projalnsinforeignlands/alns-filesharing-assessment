package de.com.alns.codingtests.leanapps.filesharing.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.com.alns.commons.base.models.AbstractPersistentModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * A File Uploaded.
 */
@Entity
@Table(name = "TBL_FILE_UPLOADED")
@Data
@EqualsAndHashCode(callSuper = false)
public class FileUploaded extends AbstractPersistentModel<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "DES_FILENAME", nullable = false)
    private String fileName;

    @NotNull
    @Column(name = "DES_PATH_FILE", nullable = false)
    private String filePath;

    @Column(name = "DES_DOWNLOAD_URI", nullable = true)
    private String downloadURI;

    @NotNull
    @Column(name = "QTY_DOWNLOADS", nullable = false)
    private Integer qtyDownloads = 0;

    @Column(name = "NUM_SIZE", nullable = true)
    private Long size;

    @Column(name = "DES_CONTENT_TYPE", nullable = true, length = 30)
    private String contentType;

    @NotNull
    @Column(name = "IND_ACTIVE")
    private boolean isActive = false;

    @JsonIgnore
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private User userOwner;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(
            name = "TBL_SHARED_FILE",
            joinColumns = {@JoinColumn(name = "FILE_UPLOADED_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_GRANTED_EMAIL", referencedColumnName = "DES_EMAIL")})
    private List<User> usersGrantedList;


    public FileUploaded() {
        this.usersGrantedList = new ArrayList<User>();
    }


    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }


    public void addQtyDownloads(Integer pQtyAdd) {
        this.qtyDownloads += pQtyAdd;
    }


}
