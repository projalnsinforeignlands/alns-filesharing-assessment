package de.com.alns.codingtests.leanapps.filesharing.services.dtos;

import de.com.alns.codingtests.leanapps.filesharing.models.FileUploaded;
import de.com.alns.commons.base.dtos.AbstractBaseDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * A DTO for Files Available for Current Logged User.
 */
@NoArgsConstructor
public class FilesAvailableForCurrentUserDTO extends AbstractBaseDTO {

    private static final long serialVersionUID = 1L;

    @Getter @Setter
    private String email;
    @Getter @Setter
    private List<FileUploaded> ownFilesUploadedList;
    @Getter @Setter
    private List<FileUploaded> filesUploadedSharedWithMeList;


}
