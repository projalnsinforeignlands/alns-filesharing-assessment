package de.com.alns.codingtests.leanapps.filesharing.repositories;

import de.com.alns.codingtests.leanapps.filesharing.models.FileUploaded;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the FileUploaded entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FileUploadedRepository extends JpaRepository<FileUploaded, String> {
	

}
