package de.com.alns.codingtests.leanapps.filesharing.services.impl;

import de.com.alns.codingtests.leanapps.filesharing.models.User;
import de.com.alns.codingtests.leanapps.filesharing.repositories.UserRepository;
import de.com.alns.codingtests.leanapps.filesharing.services.IUserService;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.NewUserDTO;
import de.com.alns.commons.base.exceptions.ApplicationGenericException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing User.
 */
@Service
@Transactional
public class UserServiceImpl implements IUserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository userRepository;


    public UserServiceImpl(UserRepository pUserRepository) {
        this.userRepository = pUserRepository;
    }

    @Override
    @Transactional
    public String createUser(NewUserDTO pNewUserDTO) {

        User userToCreate = null;
        String newUserId = null;

        try {

            userToCreate = new User();
            userToCreate.setEmail(pNewUserDTO.getEmail());
            userToCreate.setPassword(pNewUserDTO.getPassword());
            userToCreate.setIsActive(true);

            userToCreate = userRepository.save(userToCreate);

            newUserId = userToCreate.getId();
            // mailService.sendCreationEmail(newUser);

        } catch (Exception e) {
            log.debug(e.getMessage());
            throw new ApplicationGenericException(e);
        }

        return newUserId;
    }


    @Override
    public Optional<User> searchUserByEmail(String pEmailGrantedUser) {

        return userRepository.findById(pEmailGrantedUser);
    }


    @Override
    public User save(User pUserGrantedFound) {
        return userRepository.save(pUserGrantedFound);
    }

}
