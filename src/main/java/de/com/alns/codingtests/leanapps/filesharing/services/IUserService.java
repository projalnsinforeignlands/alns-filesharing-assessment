package de.com.alns.codingtests.leanapps.filesharing.services;

import de.com.alns.codingtests.leanapps.filesharing.models.User;
import de.com.alns.codingtests.leanapps.filesharing.services.dtos.NewUserDTO;

import java.util.Optional;

/**
 * Service Interface for managing User.
 */
public interface IUserService {

    public String createUser(NewUserDTO pNewUserDTO);

    public User save(User pUserGrantedFound);

    public Optional<User> searchUserByEmail(String pEmailGrantedUser);
}
