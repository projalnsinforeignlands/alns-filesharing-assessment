package de.com.alns.codingtests.leanapps.filesharing.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.com.alns.commons.base.models.AbstractPersistentModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * A user.
 */
@Entity
@Table(name = "TBL_USER")
@Data
@EqualsAndHashCode(callSuper = false)
public class User extends AbstractPersistentModel<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Email
    @Size(min = 5, max = 255)
    @Column(name = "DES_EMAIL", length = 255, unique = true, nullable = false)
    private String email;

    @JsonIgnore
    @NotNull
    @Size(min = 8, max = 20)
    @Column(name = "DES_PASSWORD", length = 60, nullable = false)
    private String password;

    @Size(max = 50)
    @Column(name = "DES_FIRST_NAME", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "DES_LAST_NAME", length = 50)
    private String lastName;

    @NotNull
    @Column(name = "IND_ACTIVE", length = 50)
    private Boolean isActive = false;


    @OneToMany(mappedBy = "userOwner")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<FileUploaded> ownFilesUploadedList;

    @ManyToMany
    @JoinTable(
        name = "TBL_SHARED_FILE",
        joinColumns = {@JoinColumn(name = "USER_GRANTED_EMAIL", referencedColumnName = "DES_EMAIL")},
        inverseJoinColumns = {@JoinColumn(name = "FILE_UPLOADED_ID", referencedColumnName = "ID")})
    @BatchSize(size = 20)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<FileUploaded> filesUploadedSharedWithMeList;


    public User() {
        this.ownFilesUploadedList = new ArrayList<FileUploaded>();
        this.filesUploadedSharedWithMeList = new ArrayList<FileUploaded>();
    }


    public String getId() {
        return email;
    }
    public void setId(String id) {
        this.email = id;
    }

}
