package de.com.alns.codingtests.leanapps.filesharing;

import de.com.alns.codingtests.leanapps.filesharing.configuration.ApplicationStartupRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ALNSFileSharingJAApplication extends SpringBootServletInitializer {

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ALNSFileSharingJAApplication.class);
	}


	public static void main(String[] args) {
		SpringApplication.run(ALNSFileSharingJAApplication.class, args);
	}


	@Bean
    public ApplicationStartupRunner bootstrapApplicationFirstTasksRunner() {
		return new ApplicationStartupRunner();
    }

}