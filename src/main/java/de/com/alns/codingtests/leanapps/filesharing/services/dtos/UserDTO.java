package de.com.alns.codingtests.leanapps.filesharing.services.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.com.alns.codingtests.leanapps.filesharing.models.FileUploaded;
import de.com.alns.commons.base.dtos.AbstractBaseDTO;
import de.com.alns.commons.base.models.AbstractPersistentModel;
import lombok.*;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * A DTO for User.
 */
@NoArgsConstructor
public class UserDTO extends AbstractBaseDTO {

    private static final long serialVersionUID = 1L;

    @Getter @Setter
    private Long id;
    @Getter @Setter
    private String email;
    @Getter @Setter
    private String password;
    @Getter @Setter
    private String firstName;
    @Getter @Setter
    private String lastName;
    @Getter @Setter
    private Boolean isActive = false;
    @Getter @Setter
    private List<FileUploaded> ownFilesUploadedList;
    @Getter @Setter
    private List<FileUploaded> filesUploadedSharedWithMeList;


}
