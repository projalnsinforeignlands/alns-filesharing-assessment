# Project ALNS-FileSharing-JA

## What Is It?

This is a Java Test Assessment implemented by me for a position advertised on UpWork Platform.
It's a simple File Uploading and Sharing control with some API's requiring HTTP Basic Authentication. The specification was sent by Lean Apps contractor.
This was implemented using:

* Java 8

* Maven 3.6.2

* Spring Initzr

* Spring-Boot 2

* Spring Data JPA

* Spring Security

* REST API's

* Swagger-UI 

* H2 Database.

## How to Execute?

Make sure you have the following installed:

* Java 8 (JDK) or newer;
* Maven 3.6;
* Git Client;

Execute the following steps:

1. Clone the project on BitBucket.org:
```shell
git clone https://scout23DF@bitbucket.org/projalnsinforeignlands/alns-filesharing-assessment.git
```
2. Go to the root of the project cloned and issue the command:
```shell
> mvn -DskipTests clean package
> java -jar ./target/alns-filesharing-assessment.jar --com.demo.uploads.directory=./some-dir-you-have-permission
```

### Notes:

```sql
# If you don't issue the "com.demo.uploads.directory" argument, it will be used the property set in "application.properties" file inside the App;

# The App should be running at: http://localhost:9090

# When the App bootstraped, was created an User with this credentials: 

>>> User: admin@system.com
>>> Password: 12345678
```

3. Test the required REST API's by:

```shell

>>> Opening your browser and pointing to Swagger-UI:

** http://localhost:9090/swagger-ui.html#/

** Using Postman;

** Using curl utility.
```

4. According to Test Assessment specification, the available REST API's implemented are:

```java
* Public API's:

** POST | /register

* Requires Authentication (HTTP Basic):

** GET  | /api/users/{pUserId}

** POST | /api/file  : Uploads a file to be owned by current authenticated User.

** POST | /api/share : Share a file with a granted User.

** GET  | /api/file  : List of all Files the current authenticated User owns or is granted to;

** GET  | /api/file/{pFileToDownloadId} : Downloads a file.
```

## ROADMAP

* Improve Unit and Integration Tests;
* When Uploading Files, make bytes writing in assychronous way (RabbitMQ / Kafka ???);
* Send e-mails when run an action.



